const requestJson = require('request-json'),
    {
        logger,
        http_request_handler
    } = require('techubank-commons'),
    BaseController = require('./base_controller.class'),
    openweathermap = requestJson.createClient('https://api.openweathermap.org')

/**
 * 
 */
module.exports = class WeatherController extends BaseController {
    constructor() {
        super()
    }

    /**
     * 
     * @param {object} req 
     * @param {object} res 
     */
    getCurrentWeather(req, res) {
        logger.info(`Fetch weather info ${JSON.stringify(req.query)}`)

        return openweathermap.get(`data/2.5/weather?lat=${req.query.lat}&lon=${req.query.lon}&units=metric&appid=${req.query.appid}`, function (err_post_mlab, res_post_mlab) {
            return http_request_handler(err_post_mlab, res_post_mlab, {
                success_handler: _getWeather_success_handler,
                error_handler: _getWeather_error_handler,
                unauthorized_handler: _getWeather_error_handler
            }, res)
        })
    }
}

function _getWeather_error_handler(req, res, callback) {
    callback.send(res.body)
}

function _getWeather_success_handler(req, res, callback) {
    const weather_info = JSON.parse(res.body),
        response_body = {
            location: weather_info.name,
            weather: weather_info.weather[0].description,
            icon: `http://openweathermap.org/img/w/${weather_info.weather[0].icon}.png`,
            temperature: weather_info.main.temp
        }
    callback.send(response_body)
}