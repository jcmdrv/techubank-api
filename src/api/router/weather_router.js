const express = require('express'),
    weather_router = express.Router(),
    WeatherController  = require('../controller/weather_controller.class'), 
    weather_controller = new WeatherController(),
    {
        environment,
        enable_cors
    } = require('techubank-commons')
 

if (environment.value.enableCors){
    weather_router.use(enable_cors)
} 


weather_router.get('/', weather_controller.getCurrentWeather)



module.exports = weather_router