/* global describe it */
const sinon = require('sinon'),
    UserController = require('../../src/api/controller/user_controller.class'),
    assert = require('assert')

describe('UserApi', function () {
    describe('checkOptions', function () {
        it('should call checkOptions successfully', function (done) {
            const req = {
                    params: ['a', 'b']
                },
                res = {
                    statusCode: 200,
                    send: (arg) => {
                        return arg
                    }
                }

            UserController.options(req, res).then((result) => {
                assert.equal(result, 'OK')
                done()
            })
        })
    })

    describe('createUser', function () {
        it('should call createUser successfully', function (done) {
            const req = {
                    params: {
                        username: 'dummy'
                    },
                    body: {
                        username: 'mynickname',
                        first_name: 'Name',
                        last_name: 'Surname',
                        nif: '12345678Y',
                        birth_date: 'string',
                        phone: '+34 666-66-66-66',
                        email: 'user@bbva.com',
                        password: 'secret',
                        status: 'ACTIVE',
                        register_date: '03/06/1978',
                        user_status: 'ACTIVE',
                        address: {
                            street: 'string',
                            zip_code: 'string',
                            city: 'string',
                            country: 'string'
                        }
                    }
                },
                res = {
                    statusCode: 200,
                    send: (arg) => {
                        return arg
                    }
                }

            const user = new UserController()

            const callback = sinon.stub(user, 'client_get')
            callback.returns(new Promise((resolve) => {
                return resolve({
                    statusCode: 200,
                    body: JSON.stringify(require('../resources/user')),
                    send: (data) => {
                        return data
                    }
                })
            }))

            const callback_post = sinon.stub(user, 'client_post')
            callback_post.returns(new Promise((resolve) => {
                return resolve({
                    statusCode: 200,
                    body: require('../resources/account'),
                    send: (data) => {
                        return data
                    }
                })
            }))


            const callback_put = sinon.stub(user, 'client_put')
            callback_put.returns(new Promise((resolve) => {
                return resolve({
                    statusCode: 200,
                    body: 'Password reset',
                    send: (data) => {
                        return data
                    }
                })
            }))

            const callback_token = sinon.stub(user, 'load_token')
            callback_token.returns(new Promise((resolve) => {
                const token = '1234567898765432'
                return resolve(token)
            }))

            user.load_token()

            user._signUpUser(req, res).then((data) => {
                assert.equal(data.user.holder_nif, '12323345Y')
                assert.equal(data.account.iban, 'ES0201823123459341733269')
                done()
            })
        })
    })
    /*
      describe('getUserByName', function () {
          it('should call getUserByName successfully', function (done) {
              //uncomment below and update the code to test getUserByName
              //instance.getUserByName(function(error) {
              //  if (error) throw error
              //expect().to.be()
              //})
              done()
          })
      })
      describe('loginUser', function () {
          it('should call loginUser successfully', function (done) {
              //uncomment below and update the code to test loginUser
              //instance.loginUser(function(error) {
              //  if (error) throw error
              //expect().to.be()
              //})
              done()
          })
      })
      describe('logoutUser', function () {
          it('should call logoutUser successfully', function (done) {
              //uncomment below and update the code to test logoutUser
              //instance.logoutUser(function(error) {
              //  if (error) throw error
              //expect().to.be()
              //})
              done()
          })
      }) */

    describe('resetPassword', function () {
        it('should call resetPassword successfully', function (done) {
            const req = {
                    params: {
                        username: 'dummy'
                    }
                },
                res = {
                    statusCode: 200,
                    send: (arg) => {
                        return arg
                    }
                }

            const user = new UserController()

            const callback = sinon.stub(user, 'client_get')
            callback.returns(new Promise((resolve) => {
                return resolve({
                    statusCode: 200,
                    body: JSON.stringify(require('../resources/user')),
                    send: (data) => {
                        return data
                    }
                })
            }))

            const callback_put = sinon.stub(user, 'client_put')
            callback_put.returns(new Promise((resolve) => {
                return resolve({
                    statusCode: 200,
                    body: 'Password reset',
                    send: (data) => {
                        return data
                    }
                })
            }))

            const callback_token = sinon.stub(user, 'load_token')
            callback_token.returns(new Promise((resolve) => {
                const token = '1234567898765432'
                return resolve(token)
            }))

            user.load_token()

            user._resetPassword(req, res).then(() => {
                assert.equal(1, 1)
                done()
            })

        })
    })

    /*     describe('updateUser', function () {
            it('should call updateUser successfully', function (done) {
                //uncomment below and update the code to test updateUser
                //instance.updateUser(function(error) {
                //  if (error) throw error
                //expect().to.be()
                //})
                done()
            })
        }) */
})