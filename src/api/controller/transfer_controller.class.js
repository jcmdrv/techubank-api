const {
        logger,
        propagate_error_response
    } = require('techubank-commons'),
    BaseController = require('./base_controller.class')


module.exports = class TransferController extends BaseController {
    constructor() {
        super()
    }

    static newTransfer(req, res) {
        const transfer_controller = new TransferController()

        return transfer_controller.load_token().then(() => {
            return transfer_controller._newTransfer(req, res)
        })
    }
    /**
     * ES0201823123457440225151
     * ES0201823123452414376461
     * 
     * @param {object} req 
     * @param {object} res 
     */
    _newTransfer(req, res) {
        const transfer_body = req.body

        logger.info(`Making a new transfer from ${transfer_body.from_iban} to ${transfer_body.to_iban} `)

        return this.client_post('/v1/transfer', transfer_body).then((res_post_mlab) => {
            return this.request_handler.call(this, res_post_mlab, {
                success_handler: this._transfer_success_handler,
                error_handler: this._transfer_error_handler,
                unauthorized_handler: this._transfer_unauthorized_handler
            }, res)
        }).catch((error) => {
            logger.error(`_newTransfer: ${error}`)
            return propagate_error_response('accounts-error', error, '500', res)
        })
    }

    _transfer_success_handler(resp, callback) {
        logger.info('_transfer_success_handler')
        callback.statusCode = resp.statusCode
        return callback.send(resp.body)
    }
    
    _transfer_error_handler(res, callback) {
        logger.info('_transfer_error_handler')
        return propagate_error_response(res.body.internal_code, res.body.message, res.statusCode, callback)
    }
    
    _transfer_unauthorized_handler(res, callback) {
        logger.info('_transfer_unauthorized_handler')
        return propagate_error_response('unauthorized', res.body, res.statusCode, callback)
    }
}
