const 
    {
        logger,
        propagate_error_response
    } = require('techubank-commons'),
    BaseController = require('./base_controller.class')


module.exports = class AccountController extends BaseController {
    constructor() {
        super()
    }

    static getUserAllAccounts(req, res) {
        const account_controller = new AccountController()

        return account_controller.load_token().then(() => {
            return account_controller._getUserAllAccounts(req, res)
        })        
    }

    static getAccountMovements(req, res) {
        const account_controller = new AccountController()
 
        return account_controller.load_token().then(() => {
            return account_controller._getAccountMovements(req, res)
        })          
    }

    static getUserAccountInfo(req, res) {
        const account_controller = new AccountController()
   
        return account_controller.load_token().then(() => {
            return account_controller._getUserAccountInfo(req, res)
        })          
    }
    

    /**
     * The username is in the request req.params['username']
     * 
     * @param {object} req 
     * @param {object} res 
     */
    _getUserAllAccounts(req, res) {
        logger.info(`Trying to get accounts who owner is ${req.params.nif}`)
        return this.client_get(`/v1/account/holder/${req.params.nif}`).then((res_post_mlab) => {
            return this.request_handler.call(this, res_post_mlab, {
                success_handler: this._accounts_success_handler,
                error_handler: this._accounts_error_handler,
                notfound_handler: this._accounts_notfound_handler
            }, res)
        }).catch((error) => {
            logger.error(`_getUserAllAccounts: ${error}`)
            propagate_error_response('accounts-error', error, '500', res)
        })
    }
     
    /**
     * 
     * @param {*} req 
     * @param {*} res 
     */
    _getAccountMovements(req, res) {
        logger.info(`Trying to get accounts who owner is ${req.params.iban}`)
        return this.client_get(`/v1/movement/account/${req.params.iban}`).then((res_get_mlab) => {
            return this.request_handler.call(this, res_get_mlab, {
                success_handler: this._accounts_success_handler,
                error_handler: this._accounts_error_handler,
                notfound_handler: this._accounts_notfound_handler
            }, res)
        }).catch((error) => {
            logger.error(`_getAccountMovements: ${error}`)
            propagate_error_response('accounts-error', error, '500', res)
        })
    }
    
    /**
     * 
     * @param {*} req 
     * @param {*} res 
     */
    _getUserAccountInfo(req, res) {
        logger.info(`Getting account's info with iban ${req.params.iban}`)
        return this.client_get(`/v1/account/iban/${req.params.iban}`).then((res_get_mlab) => {
            return this.request_handler.call(this, res_get_mlab, {
                success_handler: this._accounts_success_handler,
                error_handler: this._accounts_error_handler,
                notfound_handler: this._accounts_notfound_handler
            }, res)
        }).catch((error) => {
            logger.error(`_getUserAccountInfo: ${error}`)
            propagate_error_response('accounts-error', error, '500', res)
        })
    }

    _accounts_success_handler(res, callback) {
        callback.send(res.body)
    }
    
    _accounts_error_handler(res, callback) {
        propagate_error_response('accounts-error', res.body, '500', callback)
    }
    
    
    _accounts_notfound_handler(res, callback) {
        propagate_error_response('accounts-notfound', res.body, '500', callback)
    }
    
}

