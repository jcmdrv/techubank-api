const {
        environment,
        logger,
        propagate_error_response
    } = require('techubank-commons'),
    BaseController = require('./base_controller.class'),
    moment = require('moment')


module.exports = class UserController extends BaseController {
    constructor() {
        super()
    }

    static getUserByUsername(req, res) {
        const user_controller = new UserController()

        return user_controller.load_token().then(() => {
            return user_controller._getUserByUsername(req, res)
        })
    }

    static signUpUser(req, res) {
        const user_controller = new UserController()

        return user_controller.load_token().then(() => {
            return user_controller._signUpUser(req, res)
        })
    }

    static updateUser(req, res) {
        const user_controller = new UserController()

        return user_controller.load_token().then(() => {
            return user_controller._updateUser(req, res)
        })
    }

    static login(req, res) {
        const user_controller = new UserController()

        return user_controller.load_token().then(() => {
            return user_controller._login(req, res)
        })
    }

    static logout(req, res) {
        const user_controller = new UserController()

        return user_controller.load_token().then(() => {
            return user_controller._logout(req, res)
        })
    }

    static resetPassword(req, res) {
        const user_controller = new UserController()

        return user_controller.load_token().then(() => {
            return user_controller._resetPassword(req, res)
        })
    }

    /**
     * The username is in the request req.params['username']
     * 
     * @param {object} req 
     * @param {object} res 
     */
    _getUserByUsername(req, res) {
        logger.info(`Fetch user ${req.params.username}`)

        return this.client_get(`/v1/user/${req.params.username}/info`).then((res_post_mlab) => {
            return this.request_handler.call(this, res_post_mlab, {
                success_handler: this._getUser_success_handler,
                error_handler: this._getUser_error_handler,
                unauthorized_handler: this._getUser_error_handler
            }, res)
        })
    }

    /**
     * 
     * @param {object} req 
     * @param {object} res 
     */
    _signUpUser(req, res) {
        logger.info(`Trying to register a new user ${req.body.username}`)
        req.body.scopes = ['user']
        return this.client_post('/v1/user', req.body).then((res_post_mlab) => {
            return this.request_handler.call(this, res_post_mlab, {
                success_handler: this._signUpUser_success_handler,
                error_handler: this._signUpUser_error_handler,
                unauthorized_handler: this._signUpUser_unauthorized_handler
            }, res)
        }).catch((error) => {
            logger.error(`_signUpUser: ${error}`)
            propagate_error_response('signup-error', error, '500', res)
        })
    }

    /**
     * 
     * @param {object} req 
     * @param {object} res 
     */
    _updateUser(req, res) {
        logger.info(`Trying to updateUser a user ${req.body.username}`)
        return this.client.put('/v1/user', req.body, (err_post_mlab, res_post_mlab) => {
            return this.request_handler.call(this, res_post_mlab, {
                success_handler: this._updateUser_success_handler,
                error_handler: this._updateUser_error_handler
            }, res)
        })
    }

    /**
     * 
     * @param {*} req 
     * @param {*} res 
     */
    _login(req, res) {
        logger.info(`OAuth2 login user ${req.body.username}`)

        const redirect_uri = `${environment.value.techubank_web_url}%2Findex.html%23!%2Fglobal_position`

        let authorize_url = `/oauth/authorize?response_type=code&client_id=${req.body.username}&login&scope=user&state=1234567890`

        if (environment.value.ENV === 'local' || environment.value.ENV === 'dev' || environment.value.ENV === 'docker'){
            authorize_url = `/oauth/authorize?response_type=code&client_id=${req.body.username}&redirect_uri=${redirect_uri}&login&scope=user&state=1234567890`
        }

        return this.oauth_client.get(authorize_url, (auth_err, auth_res) => {
            if (!auth_err && auth_res && auth_res.statusCode && auth_res.statusCode == 200) {
                const token_body = {
                    'grant_type': 'authorization_code',
                    'code': auth_res.body.code,
                    'client_id': req.body.username,
                    'client_secret': req.body.password,
                    'redirect_uri': redirect_uri
                }

                logger.info('Request a new token')
                //                return this.oauth_client.post(`/oauth/token?response_type=code&client_id=${req.body.username}&redirect_uri=${redirect_uri}&scope=user&state=1234567890`, token_body, (token_err, token_resp) => {
                return this.oauth_client.post(`/oauth/token?response_type=code&client_id=${req.body.username}&scope=user&state=1234567890`, token_body, (token_err, token_resp) => {
                    return this.request_handler.call(this, token_resp, {
                        success_handler: this._token_success_handler,
                        error_handler: this._token_error_handler,
                        notfound_handler: this._token_notfound_handler,
                        unauthorized_handler: this._token_unauthorized_handler
                    }, res)
                })
            } else {
                logger.error(auth_err)
                return propagate_error_response('user-authorize-error', auth_err, '500', res)
            }
        })
    }

    /**
     * 
     * @param {object} req 
     * @param {object} res 
     */
    _logout(req, res) {
        logger.info('logout user')
        this.oauth_client.headers.access_token = req.body.access_token
        this.oauth_client.headers.username = req.body.username
        this.oauth_client.get('/oauth/logout', () => {
            res.send('logged out')
        })
    }


    _resetPassword(req, res) {
        logger.info(`Reseting password to user ${req.params['username']}`)
        return this._getUserInfo(req.params['username']).then((user) => {
            user.password = '12345'
            return this._updateUserInfo.call(this, user, res)
        }).catch((error) => {
            logger.error(`_resetPassword: ${error}`)
        })
    }

    _getUserInfo(username) {
        return new Promise((resolve, reject) => {
            return this.client_get(`/v1/user/info/${username}`).then((res_get_mlab) => {
                const user = JSON.parse(res_get_mlab.body)

                user.password = '12345'
                resolve(user)
            }).catch((error) => {
                logger.error(`_getUserInfo: ${error}`)
                return reject(error)
            })
        })
    }

    _updateUserInfo(user, res) {
        return new Promise((resolve, reject) => {
            return this.client_put('/v1/user/resetPassword', user).then((res_put_mlab) => {
                this.request_handler.call(this, res_put_mlab, {
                    success_handler: this._resetPassword_success_handler,
                    error_handler: this._resetPassword_error_handler
                }, res)
                resolve('reseted')
            }).catch((error) => {
                logger.error(`Error reseting user password: ${error}`)
                reject(error)
            })
        })
    }

    _token_success_handler(res, callback) {
        logger.debug(`Token for user ${res.body.username} received`)
        callback.send(res.body)
    }

    _token_error_handler(res, callback) {
        return propagate_error_response('user-token-error', res.body, res.statusCode, callback)
    }

    _token_notfound_handler(res, callback) {
        return propagate_error_response('user-notfound-error', res.body, res.statusCode, callback)
    }

    _token_unauthorized_handler(res, callback) {
        return propagate_error_response('unauthorized', res.body, res.statusCode, callback)
    }

    _account_success_handler(res, callback) {
        return callback.send({
            user: res.body,
            account: res.body
        })
    }


    _account_error_handler(res, callback) {
        return propagate_error_response('signup-error', res.body, res.statusCode, callback)
    }

    _signUpUser_error_handler(res, callback) {
        return propagate_error_response('signup-update-error', res.body, res.statusCode, callback)
    }

    _signUpUser_unauthorized_handler(res, callback) {
        return propagate_error_response('unauthorized', res.body, res.statusCode, callback)
    }


    _updateUser_success_handler(res, callback) {
        return callback.send(res.body)
    }


    _updateUser_error_handler(res, callback) {
        return propagate_error_response('update-error', res.body, res.statusCode, callback)
    }

    _signUpUser_success_handler(res, callback) {
        logger.info(`Creating a new Account to user ${res.body.username}`)

        const account = {
            'title': `${res.body.name} opening account`,
            'holder_nif': res.body.nif,
            'co_holder_nif': '',
            'type': 'CHECKING',
            'opening_date': moment(new Date()).format('DD-MM-YYYY'),
            'status': 'ACTIVE',
            'description': 'Checking account',
            'currency': 'EUR',
            'available_balance': '0',
            'current_balance': '100',
            'correspondence_address': {
                'street': 'Calle Batanes 3',
                'zip_code': '28760',
                'city': 'Tres Cantos',
                'country': 'Spain'
            }
        }

        return this.client_post('/v1/account', account).then((res_post_mlab) => {
            return this.request_handler.call(this, res_post_mlab, {
                success_handler: this._account_success_handler,
                error_handler: this._account_error_handler
            }, callback)
        }).catch((error) => {
            logger.error(`_signUpUser_success_handler: ${error}`)
            propagate_error_response('signup-error', error, '500', res)
        })

    }

    _getUser_error_handler(res, callback) {
        return propagate_error_response('user-error', res.body, res.statusCode, callback)
    }

    _getUser_success_handler(res, callback) {
        return callback.send(res.body)
    }

    _resetPassword_success_handler(res, callback) {
        return callback.send('Password reset')
    }

    _resetPassword_error_handler(res, callback) {
        return propagate_error_response('reset-password-error', res.body, res.statusCode, callback)
    }

}