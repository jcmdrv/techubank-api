const express = require('express'),
    account_router = express.Router(),
    AccountController  = require('../controller/account_controller.class'),
    {
        environment,
        enable_cors,
        check_security_headers
    } = require('techubank-commons')

// Add OAuth Security Headers
account_router.use(check_security_headers)     

if (environment.value.enableCors){
    account_router.use(enable_cors)
} 


account_router.options(AccountController.options)
account_router.get('/holder/:nif', AccountController.getUserAllAccounts)
account_router.get('/:iban/movement', AccountController.getAccountMovements)
account_router.get('/:iban/info', AccountController.getUserAccountInfo)

module.exports = account_router
