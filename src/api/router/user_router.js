const express = require('express'),
    user_router = express.Router(),
    UserController = require('../controller/user_controller.class'),
    {
        environment,
        enable_cors,
        check_security_headers
    } = require('techubank-commons')

// Add OAuth Security Headers
user_router.use('/:username/info', check_security_headers)
user_router.use('/:username/logout', check_security_headers)


if (environment.value.enableCors) {
    user_router.use(enable_cors)
}


user_router.options(UserController.options)
user_router.post('/', UserController.signUpUser)
user_router.put('/', UserController.updateUser)
user_router.post('/login', UserController.login)
user_router.get('/:username/info', UserController.getUserByUsername)
user_router.options('/:username/info', UserController.options)
user_router.post('/:username/logout', UserController.logout)
user_router.get('/:username/resetPassword', UserController.resetPassword)


module.exports = user_router