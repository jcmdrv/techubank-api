require('dotenv').config()

const pjson = require('../package.json'),
    path = require('path'),
    fs = require('fs'),
    express = require('express'),
    bodyParser = require('body-parser'),
    app = express(),
    port = 3000,
    port_ssl = 3443,
    user_router = require('./api/router/user_router'),
    account_router = require('./api/router/account_router'),
    transfer_router = require('./api/router/transfer_router'),
    weather_router = require('./api/router/weather_router'),
    {
        logger,
        enable_cors,
        environment
    } = require('techubank-commons')


if (environment.value.enableCors) {
    app.use(enable_cors)
}

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
    extended: false
}))

// parse application/json
app.use(bodyParser.json())

app.use('/ws-doc', express.static(path.join(__dirname, '..', 'ws-doc')))
app.use('/v2/user', user_router)
app.use('/v2/account', account_router)
app.use('/v2/transfer', transfer_router)
app.use('/v2/weather', weather_router)

// CHECK IF COMMUNICATION IS OVER HTTP OR HTTPS
//if (environment.value.protocol === 'https') {
const https = require('https'),
    helmet = require('helmet')

app.use(helmet()) // Add Helmet as a middleware
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0 // this allows self-signed cer
https.createServer({
    key: fs.readFileSync('key.pem'),
    cert: fs.readFileSync('certificate.pem')
}, app).listen(port_ssl)
//} else {
app.listen(port)
//}

logger.info(`***** techubank-api@${pjson.version} *************`)
logger.info(`App listening HTTP on port ${port}`)
logger.info(`App listening HTTPS on port ${port_ssl}`)





