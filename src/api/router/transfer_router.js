const express = require('express'),
    transfer_router = express.Router(),
    TransferController  = require('../controller/transfer_controller.class'), 
    {
        environment,
        enable_cors,
        check_security_headers
    } = require('techubank-commons')

// Add OAuth Security Headers
transfer_router.use(check_security_headers) 

if (environment.value.enableCors) {
    transfer_router.use(enable_cors)
}

transfer_router.post('/', TransferController.newTransfer)


module.exports = transfer_router